package main

import (
	"reflect"
	"testing"
)

func Test_makePathsRelative(t *testing.T) {
	in := []byte(`[{"filePath":"/full/path/to/project/js/main.js"}]`)
	want := []byte(`[{"filePath":"js/main.js","messages":null}]`)
	got, err := makePathsRelative(in, "/full/path/to/project")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%s\nbut got:\n%s", want, got)
	}
}
