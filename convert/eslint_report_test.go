package convert

import (
	"reflect"
	"testing"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

var fileReport = ESLintFileReport{
	FilePath: "js/main.js",
	Messages: []ESLintVulnerability{
		{
			Line:    7,
			EndLine: 8,
			Message: "ABCD",
			RuleID:  "security/test",
			Source:  "alert('Foo');",
		},
	},
}

func TestBugInstance_CompareKey(t *testing.T) {
	want := "js/main.js:dae902368c7efca951e45b29344687ff523068d06760550adbec87d65671b589:security/test"
	got := fileReport.Messages[0].compareKey(fileReport)
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}

func TestBugInstance_Location(t *testing.T) {
	tests := []struct {
		name        string
		FileReport  ESLintFileReport
		prependPath string
		want        report.Location
	}{
		{
			name:       "No prepend path",
			FileReport: fileReport,
			want: report.Location{
				File:      "js/main.js",
				LineStart: 7,
				LineEnd:   8,
			},
		},
		{
			name:        "With prepend path",
			FileReport:  fileReport,
			prependPath: "app",
			want: report.Location{
				File:      "app/js/main.js",
				LineStart: 7,
				LineEnd:   8,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.FileReport.Messages[0].location(tt.prependPath, tt.FileReport)

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", tt.want, got)
			}
		})
	}
}

func TestBugInstance_Identifiers(t *testing.T) {
	want := report.Identifier{
		Type:  "eslint_rule_id",
		Name:  "ESLint rule ID security/test",
		Value: "security/test",
		URL:   "https://github.com/nodesecurity/eslint-plugin-security#test",
	}
	got := fileReport.Messages[0].eSLintIdentifier()
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
