package convert

import (
	"errors"
	"strconv"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	cweinfo "gitlab.com/gitlab-org/security-products/cwe-info-go"
)

// CWE is the Common Weakness Enumeration designation for the relevant rule
// wrapping CweInfo and attaching a Mitre URL and vuln severity
type CWE struct {
	cweinfo.CweInfo
	Severity report.SeverityLevel
}

// ErrNoCWE indicates no CWE was found for matching ruleID
var ErrNoCWE = errors.New("No CWE found")

// idToCWE returns a Common Weakness Enumeration matching eslint rule ID
func idToCWE(id int) (CWE, error) {
	info, err := cweinfo.GetCweInfo(strconv.Itoa(id))
	if err != nil {
		return CWE{}, ErrNoCWE
	}

	severity := report.SeverityLevelUnknown
	if sev, ok := cweSeverities[id]; ok {
		severity = sev
	}

	return CWE{
		CweInfo:  info,
		Severity: severity,
	}, nil
}

var cweSeverities = map[int]report.SeverityLevel{
	22:   report.SeverityLevelHigh,
	74:   report.SeverityLevelCritical,
	73:   report.SeverityLevelMedium,
	78:   report.SeverityLevelCritical,
	79:   report.SeverityLevelMedium,
	94:   report.SeverityLevelCritical,
	95:   report.SeverityLevelCritical, // Calculated from parent CWE-94
	185:  report.SeverityLevelMedium,
	208:  report.SeverityLevelLow,
	269:  report.SeverityLevelHigh,
	338:  report.SeverityLevelHigh,
	352:  report.SeverityLevelHigh,
	400:  report.SeverityLevelHigh, // Calculated from parent CWE-664
	664:  report.SeverityLevelHigh,
	1022: report.SeverityLevelHigh, // Calculated from parent CWE-269
}
