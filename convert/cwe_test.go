package convert

import (
	"testing"

	"github.com/stretchr/testify/assert"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
)

func TestidToCWE(t *testing.T) {
	tests := []struct {
		id           int
		wantSeverity report.SeverityLevel
		wantErr      error
	}{
		{
			id:           22,
			wantSeverity: report.SeverityLevelHigh,
		},
		{
			id:           78,
			wantSeverity: report.SeverityLevelCritical,
		},
		{
			id:      0,
			wantErr: ErrNoCWE,
		},
	}

	for _, tt := range tests {
		got, err := idToCWE(tt.id)
		if tt.wantErr != nil {
			if err == nil {
				t.Fatal("Wrong result. Expected error but no error found")
			}
			assert.Equal(t, tt.wantErr, err, "should be equal")
		}
		if tt.wantErr == nil && err != nil {
			t.Fatalf("received error: %v", err)
		}
		assert.Equal(t, tt.wantSeverity, got.Severity, "should be equal")
	}
}
