package convert

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/security-products/analyzers/eslint/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

func TestConvert(t *testing.T) {
	in := `[
    {
        "filePath": "html/index.html",
        "messages": [
            {
                "endLine": 8,
                "line": 8,
                "message": "Unsafe Regular Expression",
                "ruleId": "security/detect-unsafe-regex",
                "source": "     var emailExpression = /^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$/;"
            }
        ]
    },
    {
        "filePath": "js/main.js",
        "messages": [
            {
                "endLine": 25,
                "line": 25,
                "message": "Generic Object Injection Sink",
                "ruleId": "security/detect-object-injection",
                "source": ""
            },
            {
                "endLine": 49,
                "line": 47,
                "message": "Potential timing attack, right side: true",
                "ruleId": "security/detect-possible-timing-attacks",
                "source": ""
            },
			{
                "endLine": 55,
                "line": 55,
                "message": "New unknown security rule",
                "ruleId": "security/new-rule",
                "source": ""
            }
        ]
    }
	]`

	var scanner = metadata.IssueScanner

	r := strings.NewReader(in)
	want := &report.Report{
		Version: report.CurrentVersion(),
		Vulnerabilities: []report.Vulnerability{

			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        "Unsafe Regular Expression",
				Message:     "Unsafe Regular Expression",
				Description: "Potentially unsafe regular expressions. It may take a very long time to run.",
				CompareKey:  "html/index.html:b084550886ce03d51d5213138cb086e85c90cea96b2a6ba2de9453fb75da279e:security/detect-unsafe-regex",
				Severity:    report.SeverityLevelHigh,
				Confidence:  report.ConfidenceLevelUnknown,
				Location: report.Location{
					File:      "html/index.html",
					LineStart: 8,
					LineEnd:   8,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "eslint_rule_id",
						Name:  "ESLint rule ID security/detect-unsafe-regex",
						Value: "security/detect-unsafe-regex",
						URL:   "https://github.com/nodesecurity/eslint-plugin-security#detect-unsafe-regex",
					},
					{
						Type:  "cwe",
						Name:  "CWE-400",
						Value: "400",
						URL:   "https://cwe.mitre.org/data/definitions/400.html",
					},
				},
				Links: []report.Link{},
			},
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        "Generic Object Injection Sink",
				Message:     "Generic Object Injection Sink",
				Description: "Bracket object notation with user input is present, this might allow an attacker to access all properties of the object and even it's prototype, leading to possible code execution.",
				CompareKey:  "js/main.js:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855:security/detect-object-injection",
				Severity:    report.SeverityLevelCritical,
				Confidence:  report.ConfidenceLevelUnknown,
				Location: report.Location{
					File:      "js/main.js",
					LineStart: 25,
					LineEnd:   25,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "eslint_rule_id",
						Name:  "ESLint rule ID security/detect-object-injection",
						Value: "security/detect-object-injection",
						URL:   "https://github.com/nodesecurity/eslint-plugin-security#detect-object-injection",
					},
					{
						Type:  "cwe",
						Name:  "CWE-94",
						Value: "94",
						URL:   "https://cwe.mitre.org/data/definitions/94.html",
					},
				},
				Links: []report.Link{},
			},
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        "Potential timing attack, right side: true",
				Message:     "Potential timing attack, right side: true",
				Description: "Insecure comparisons (==, !=, !== and ===), which check input sequentially. This could lead to timing attacks on your application.",
				CompareKey:  "js/main.js:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855:security/detect-possible-timing-attacks",
				Severity:    report.SeverityLevelLow,
				Confidence:  report.ConfidenceLevelUnknown,
				Location: report.Location{
					File:      "js/main.js",
					LineStart: 47,
					LineEnd:   49,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "eslint_rule_id",
						Name:  "ESLint rule ID security/detect-possible-timing-attacks",
						Value: "security/detect-possible-timing-attacks",
						URL:   "https://github.com/nodesecurity/eslint-plugin-security#detect-possible-timing-attacks",
					},
					{
						Type:  "cwe",
						Name:  "CWE-208",
						Value: "208",
						URL:   "https://cwe.mitre.org/data/definitions/208.html",
					},
				},
				Links: []report.Link{},
			},
			{
				Category:    report.CategorySast,
				Scanner:     scanner,
				Name:        "New unknown security rule",
				Message:     "New unknown security rule",
				Description: "New unknown security rule",
				CompareKey:  "js/main.js:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855:security/new-rule",
				Severity:    report.SeverityLevelUnknown,
				Confidence:  report.ConfidenceLevelUnknown,
				Location: report.Location{
					File:      "js/main.js",
					LineStart: 55,
					LineEnd:   55,
				},
				Identifiers: []report.Identifier{
					{
						Type:  "eslint_rule_id",
						Name:  "ESLint rule ID security/new-rule",
						Value: "security/new-rule",
						URL:   "https://github.com/nodesecurity/eslint-plugin-security#new-rule",
					},
				},
				Links: []report.Link{},
			},
		},
		Analyzer:        "eslint",
		Config:          ruleset.Config{Path: ruleset.PathSAST},
		DependencyFiles: []report.DependencyFile{},
		Remediations:    []report.Remediation{},
	}
	got, err := Convert(r, "")
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, want, got, "should be equal")
}
