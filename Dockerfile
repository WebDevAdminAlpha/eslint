ARG SCANNER_VERSION=7.25.0

FROM golang:1.15-alpine AS gobuild

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src/buildapp
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM node:15.4-alpine3.12 as builder

ARG SCANNER_VERSION

COPY package.json /
RUN yarn && yarn upgrade "eslint@$SCANNER_VERSION"

FROM node:15.4-alpine3.12

ARG SCANNER_VERSION
ENV SCANNER_VERSION $SCANNER_VERSION

# The node user below doesn't have permission to create a file in /etc/ssl/certs
# or /etc/gitconfig, this RUN command creates files that the analyzer can write to.
RUN mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    chown root:node /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    chmod g+w /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    touch /etc/gitconfig && \
    chown root:node /etc/gitconfig && \
    chmod g+w /etc/gitconfig

USER node
WORKDIR /home/node

COPY --chown=node:node eslintrc ./.eslintrc
COPY --chown=node:node babel.config.json .
COPY --from=builder --chown=node:node yarn.lock package.json ./
COPY --from=gobuild --chown=root:root /analyzer /analyzer

RUN yarn --frozen-lockfile && yarn cache clean

ENTRYPOINT []
CMD ["/analyzer", "run"]
