# ESLint analyzer

GitLab Analyzer for Javascript projects.
This analyzer is based on the [ESLint security plugin](https://github.com/nodesecurity/eslint-plugin-security) and the [ESLint React plugin](https://www.npmjs.com/package/eslint-plugin-react). It
processes `.js`, `.jsx`, and `.html` files.

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Versioning and release process

Please check the [Release Process documentation](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/release_process.md).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
